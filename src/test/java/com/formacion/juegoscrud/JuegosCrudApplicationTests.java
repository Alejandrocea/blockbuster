package com.formacion.juegoscrud;

import static org.junit.Assert.assertTrue;

import java.time.LocalDate;

import org.junit.jupiter.api.Test;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import com.formacion.juegoscrud.entities.Cliente;
import com.formacion.juegoscrud.entities.Roles;
import com.formacion.juegoscrud.enums.enumUserRoles;
import com.formacion.juegoscrud.repository.ClienteRepository;

@SpringBootTest
class JuegosCrudApplicationTests {

	@Autowired
	private ClienteRepository clienteRepository;
	
	@Autowired
	private BCryptPasswordEncoder encoder;
	
	@Test
	public void crearUsurarioTest() {
		
		Roles rol = new Roles();
		rol.setRoles(enumUserRoles.ADMIN);
		
		Cliente clienteEnt = new Cliente();
		clienteEnt.setNombreCliente("PeterAdministrador");
		clienteEnt.setDocumentacionCliente("00000");
		clienteEnt.setCorreoCliente("Peter@peter.com");
		clienteEnt.setFechaNacimientoCliente(LocalDate.now());
		clienteEnt.setPassword(encoder.encode("123"));
		clienteEnt.setUsername("PeterAdministrador");
		clienteEnt.getRoles().add(rol);
		//rol.getClientes().add(clienteEnt);
		Cliente retorno =clienteRepository.save(clienteEnt);
		
		assertTrue(retorno.getPassword().equalsIgnoreCase(clienteEnt.getPassword()));		
	}
}
