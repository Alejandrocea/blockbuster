package com.formacion.juegoscrud.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.formacion.juegoscrud.dto.CompanyDto;
import com.formacion.juegoscrud.dto.JuegosDto;
import com.formacion.juegoscrud.dto.TiendaDto;

@Service
public interface JuegosCompanyTiendaService {
	
	//public void agregarJuegosJson();
	
	//public void agregarCompanyJson();
	
	public void agregarTiendaJson();
	
	public List<JuegosDto> getTodosJuegosBD();
	
	public JuegosDto getUnJuegoBD(String titulo);
	
	public CompanyDto getUnaCompanyBD(String nombre);
	
	public List<CompanyDto> getTodosCompany();
	
	public List<TiendaDto> getTodosTienda();
	
	public void añadirCompañia(CompanyDto compañiaDto);
	
	public void añadirJuego (JuegosDto juegoDto);
	
	
}
