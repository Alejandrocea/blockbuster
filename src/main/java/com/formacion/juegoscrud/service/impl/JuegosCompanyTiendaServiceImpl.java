package com.formacion.juegoscrud.service.impl;

import java.io.FileReader;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;

import org.json.simple.parser.JSONParser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.formacion.juegoscrud.conversiones.ConversionCompañia;
import com.formacion.juegoscrud.conversiones.ConversionJuego;
import com.formacion.juegoscrud.conversiones.ConversionTienda;
import com.formacion.juegoscrud.dto.ClientesDto;
import com.formacion.juegoscrud.dto.CompanyDto;
import com.formacion.juegoscrud.dto.JuegosDto;
import com.formacion.juegoscrud.dto.RolesDto;
import com.formacion.juegoscrud.dto.TiendaDto;
import com.formacion.juegoscrud.entities.Cliente;
import com.formacion.juegoscrud.entities.Company;
import com.formacion.juegoscrud.entities.Juegos;
import com.formacion.juegoscrud.entities.Roles;
import com.formacion.juegoscrud.entities.Tienda;
import com.formacion.juegoscrud.exceptions.exceptioncompany.CompanyNoContentException;
import com.formacion.juegoscrud.exceptions.exceptionsjuego.JuegoNoContentException;
import com.formacion.juegoscrud.exceptions.exceptionsjuego.JuegoNotFoundException;
import com.formacion.juegoscrud.repository.CompanyRepository;
import com.formacion.juegoscrud.repository.JuegosRepository;
import com.formacion.juegoscrud.repository.TiendaRepository;
import com.formacion.juegoscrud.service.JuegosCompanyTiendaService;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

@Service
public class JuegosCompanyTiendaServiceImpl implements JuegosCompanyTiendaService{
	
	Cliente cliente1 = new Cliente();
	
	@Autowired
	private JuegosRepository juegosRepository;
	
	@Autowired
	private CompanyRepository companyRepository;
	
	@Autowired
	private TiendaRepository tiendaRepository;
	
	public List<JuegosDto> listaJuegosDto = new ArrayList<>();
	public List<CompanyDto> listaCompanyDto = new ArrayList<>();
	public List<TiendaDto> listaTiendaDto = new ArrayList<>();
	public List<Company> listaCompany = new ArrayList<>();
	public List<Juegos> listaJuegos = new ArrayList<>();
	
	/*public void agregarJuegosJson() {
		JSONParser parser = new JSONParser();
		try {
			Object obj = parser.parse(new FileReader("initJuegos.json"));
			Gson gson = new Gson(); 
			
			String cadJson = gson.toJson(obj); 				
			
			Type listType = new TypeToken<ArrayList<JuegosDto>>(){}.getType();
			ArrayList<JuegosDto> arrayDeJson = gson.fromJson(cadJson, listType);
			listaJuegosDto = arrayDeJson;

			for(JuegosDto juegos : listaJuegosDto) {
				Juegos j = new Juegos();
				//Company c = companyRepository.findByNombreCompany(juegos.getCompanyDto().getNombreCompany());

				j.setTitulo(juegos.getTitulo());
				j.setFechaLanzamientoJuego(juegos.getFechaLanzamientoJuego());
				j.setPegiEdadJuego(juegos.getPegiEdadJuego());
				j.setCategoriasJuegos(juegos.getCategoriasJuegos());
				
				List<Juegos> listaJuegosEnt = new ArrayList<Juegos>();
			
				listaJuegosEnt.add(j);
				//listaCompany.add(c);
				
				//c.setJuegos(listaJuegosEnt);
				j.setCompany(listaCompany);
				
				juegosRepository.save(j);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void agregarCompanyJson() {		
		JSONParser parser = new JSONParser();
		try {
			Object obj = parser.parse(new FileReader("initCompany.json"));
			Gson gson = new Gson(); 
			
			String cadJson = gson.toJson(obj); 				
			
			Type listType = new TypeToken<ArrayList<CompanyDto>>(){}.getType();
			ArrayList<CompanyDto> arrayDeJson = gson.fromJson(cadJson, listType);
			listaCompanyDto = arrayDeJson;
			
			for(CompanyDto companys : listaCompanyDto) {
				Company c = new Company();
				c.setCifCompany(companys.getCifCompany());
				c.setNombreCompany(companys.getNombreCompany());
				companyRepository.save(c);				
				System.out.println(c);
			}			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}*/
	
	public void agregarTiendaJson() {
		JSONParser parser = new JSONParser();
		try {
			Object obj = parser.parse(new FileReader("initTienda.json"));
			Gson gson = new Gson(); 
			String cadJson = gson.toJson(obj); 				
			Type listType = new TypeToken<ArrayList<TiendaDto>>(){}.getType();
			ArrayList<TiendaDto> arrayDeJson = gson.fromJson(cadJson, listType);
			listaTiendaDto = arrayDeJson;
			
			for(TiendaDto tienda : listaTiendaDto) {
				Tienda t = new Tienda();
				t.setNombreTienda(tienda.getNombreTienda());
				t.setStockTienda(tienda.getNumeroTienda());				
				tiendaRepository.save(t);
				System.out.println(t);
			}			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	//FUNCIONA
	public List<JuegosDto> getTodosJuegosBD(){
		listaJuegosDto.clear();
		listaJuegosDto = ConversionJuego.listJuegoToListJuegoDto(juegosRepository.findAll());
		return listaJuegosDto;
	}
	
	public JuegosDto getUnJuegoBD(String titulo){
		JuegosDto juego = new JuegosDto();
		Juegos juegoExiste = juegosRepository.findByTitulo(titulo);
	
		if(juegoExiste == null)
			throw new JuegoNotFoundException("El juego no existe");
		
		juego = ConversionJuego.juegoToJuegoDto(juegosRepository.findByTitulo(titulo));
		return juego;
	}
	
	public CompanyDto getUnaCompanyBD(String nombre){
		CompanyDto companydto = new CompanyDto();
		return companydto = ConversionCompañia.compañiaToCompañiaDto(companyRepository.findByNombreCompany(nombre));
	}
	
	/*public List<CompanyDto> getTodosCompany(){
		listaCompanyDto.clear();
		
		return listaCompanyDto = ConversionCompañia.listCompañiaToListCompañiaDto(companyRepository.findAll());
	}*/
	
	
	public List<CompanyDto> getTodosCompany() {		
		listaCompanyDto.clear();
		//listaCompañiaDto = conversionCompañia.listCompañiaToListCompañiaDto(compañiaRepository.findAll());
		List<Company> listaCompañiaEnt = companyRepository.findAll();
		
		for(Company compañiaEnt : listaCompañiaEnt) {
			CompanyDto companiaDto = new CompanyDto();
			
			companiaDto.setNombreCompany(compañiaEnt.getNombreCompany());
			companiaDto.setCifCompany(compañiaEnt.getCifCompany());
			companiaDto.setJuegos(ConversionJuego.listJuegoToListJuegoDto(compañiaEnt.getJuegos()));
		
			listaCompanyDto.add(companiaDto);		
		}		
		return listaCompanyDto ;
	}
	
	public List<TiendaDto> getTodosTienda(){
		listaTiendaDto.clear();
		return listaTiendaDto = ConversionTienda.listStockToListStockDto(tiendaRepository.findAll());
	}	
	
	
	
	/**
	 * creacion company y jugeos a parte
	 */
	 
	public void añadirCompañia(CompanyDto compañiaDto) {
			Company companyEnt = ConversionCompañia.compañiaDtoToCompañia(compañiaDto);		
			companyRepository.save(companyEnt);
		} 
	
	/*public void añadirJuego (String nombre, JuegosDto juegoDto) {
		
		Juegos juegoEnt = ConversionJuego.juegoDtoToJuego(juegoDto);
		
		Company companyEnt = companyRepository.findByNombreCompany(nombre);
		
		List<Company> listaCompanyEnt = new ArrayList<Company>();
		List<Juegos> listaJuegosEnt = new ArrayList<Juegos>();

		listaJuegosEnt.add(juegoEnt);
		listaCompanyEnt.add(companyEnt);
		
		companyEnt.setJuegos(listaJuegosEnt);
		juegoEnt.setCompany(listaCompanyEnt);
		
		juegosRepository.save(juegoEnt);
		//throw new JuegoBadRequestException("has introducido mal los datos del juego");
	} */
	
public void añadirJuego (JuegosDto juegoDto) {
		
		Juegos juegoEnt = ConversionJuego.juegoDtoToJuego(juegoDto);
		List<Company> listaCompanyEnt = new ArrayList<Company>();
		for (CompanyDto companiaDto : juegoDto.getCompany()) {
			Company companiaEnt = companyRepository.findByNombreCompany(companiaDto.getNombreCompany());	
			listaCompanyEnt.add(companiaEnt);
			companiaEnt.getJuegos().add(juegoEnt);
		}
		juegoEnt.setCompany(listaCompanyEnt);	
		juegosRepository.save(juegoEnt);
	}
	
	public void modificarJuego(String titulo, JuegosDto juegoMod) {
		Juegos juegosEntidad = juegosRepository.findByTitulo(titulo);
		
		if(juegoMod.getTitulo() != null)
			juegosEntidad.setTitulo(juegoMod.getTitulo());
		if(juegoMod.getCategoriasJuegos() != null)
			juegosEntidad.setCategoriasJuegos(juegoMod.getCategoriasJuegos());
		if(juegoMod.getFechaLanzamientoJuego() != null)
			juegosEntidad.setFechaLanzamientoJuego(juegoMod.getFechaLanzamientoJuego());
		if(juegoMod.getPegiEdadJuego() != null)
			juegosEntidad.setPegiEdadJuego(juegoMod.getPegiEdadJuego());
		
		if(juegoMod.getCompany() != null) {
			List<Company> listaCompanyEnt = new ArrayList<Company>();
			for (CompanyDto compa : juegoMod.getCompany()) {
				Company companyEntidad = companyRepository.findByNombreCompany(compa.getNombreCompany());
				listaCompanyEnt.add(companyEntidad);
				companyEntidad.getJuegos().add(juegosEntidad);
			}
			juegosEntidad.setCompany(listaCompanyEnt);
		}
		juegosRepository.save(juegosEntidad);
	}
	
	public void borrarJuego(String titulo) {
		Juegos juegoEntidad = juegosRepository.findByTitulo(titulo);
		if(juegoEntidad == null)
			throw new JuegoNoContentException("El juego a borrar no existe.");
		juegosRepository.delete(juegoEntidad);	
	}
	
	public void borrarCompany(String companyName) {
		Company companyEntidad = companyRepository.findByNombreCompany(companyName);
		if(companyEntidad == null)
			throw new CompanyNoContentException("El juego a borrar no existe.");
		companyRepository.delete(companyEntidad);	
	}
}
