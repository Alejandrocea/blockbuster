package com.formacion.juegoscrud.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.formacion.juegoscrud.conversiones.ConverisionStock;
import com.formacion.juegoscrud.conversiones.ConversionCliente;
import com.formacion.juegoscrud.conversiones.ConversionJuego;
import com.formacion.juegoscrud.conversiones.ConversionTienda;
import com.formacion.juegoscrud.dto.StockDto;
import com.formacion.juegoscrud.entities.Cliente;
import com.formacion.juegoscrud.entities.Juegos;
import com.formacion.juegoscrud.entities.Stock;
import com.formacion.juegoscrud.entities.Tienda;
import com.formacion.juegoscrud.exceptions.exceptionscliente.ClienteNotFoundException;
import com.formacion.juegoscrud.exceptions.exceptionsjuego.JuegoNotFoundException;
import com.formacion.juegoscrud.exceptions.exceptionsstock.StockForbiddenException;
import com.formacion.juegoscrud.exceptions.exceptionsstock.StockNotFoundException;
import com.formacion.juegoscrud.exceptions.exceptiontienda.TiendaNotFoundException;
import com.formacion.juegoscrud.repository.ClienteRepository;
import com.formacion.juegoscrud.repository.JuegosRepository;
import com.formacion.juegoscrud.repository.StockRepository;
import com.formacion.juegoscrud.repository.TiendaRepository;
import com.formacion.juegoscrud.service.StockService;

@Service
public class StockServiceImpl implements StockService{
	
	@Autowired
	private StockRepository stockRepository;
	
	@Autowired
	private ClienteRepository clienteRepository;
	
	@Autowired
	private JuegosRepository juegosRepository;
	
	@Autowired
	private TiendaRepository tiendaRepository;
	
	public List<StockDto> listaStock = new ArrayList<>();
	public List<Stock> listaStockDB = new ArrayList<>();
	
	Logger logger = LoggerFactory.getLogger(StockServiceImpl.class); 

	public void addStockBD(String tituloJuego, String nombreTienda, String docNumber, StockDto stockDto) {
		Stock s = new Stock();
		
		//Cliente c = clienteRepository.findByDocumentacionCliente(docNumber).orElseThrow(() -> {logger.error("error logger"); return new ClienteNotFoundException("El cliente no ha sido encontrado.");});;
		Cliente c = clienteRepository.findByDocumentacionCliente(docNumber);
		Juegos j = juegosRepository.findByTitulo(tituloJuego);
		Tienda t = tiendaRepository.findByNombreTienda(nombreTienda);
		
		if (j == null)
			throw new JuegoNotFoundException("El juego no ha sido encontrado.");
		if (t == null)
			throw new TiendaNotFoundException("La tienda no ha sido encontrada.");
		if (c == null)
			throw new ClienteNotFoundException("El cliente no ha sido encontrado.");
		if(ClienteServiceImpl.calcularEdad(c.getFechaNacimientoCliente() , j.getPegiEdadJuego()))
			throw new StockForbiddenException("La edad del cliente es menor que la edad permitida para comprar o alquilar el juego.");
		if (stockDto == null)
			throw new StockNotFoundException("Los datos del stock son vacio.");
		
		s.setReferenciaPedidoStock(stockDto.getReferenciaPedidoStock());
		s.setEstadoStock(stockDto.getEstadoStock());
		
		s.setCliente(c);
		s.setJuegos(j);
		s.setTiendaObj(t);
		
		c.getStocks().add(s);
		j.getStocks().add(s);
		t.getStocks().add(s);
		
		stockRepository.save(s);
	}
	
	public StockDto getStockPorReferenciaDB(Integer referenciaStock) {
		Stock s = stockRepository.findByReferenciaPedidoStock(referenciaStock);		
		if(s == null)
			throw new StockNotFoundException("No se ha encontrado el stock.");
		StockDto stockDto = new StockDto();
		stockDto.setEstadoStock(s.getEstadoStock());
		stockDto.setReferenciaPedidoStock(s.getReferenciaPedidoStock());
		//stockDto.setReferenciaJuegoStock(s.getReferenciaJuegoStock());
		
		stockDto.setJuegos(ConversionJuego.juegoToJuegoDto(s.getJuegos()));
		stockDto.setTienda(ConversionTienda.tiendaTotiendaDto(s.getTiendaObj()));
		stockDto.setClientes(ConversionCliente.clienteDtoToCliente(s.getCliente()));
		
		return stockDto;
	}
	
	public List<StockDto> getTodosStockDB() {
		List<Stock> listaStockDB = stockRepository.findAll();
		List<StockDto> stockDto = new ArrayList<>();
		for (Stock stockEntidad : listaStockDB){			
			stockDto.add(ConverisionStock.stockToStockDto(stockEntidad));
		}
		return stockDto;
		//return listaStock = ConverisionStock.listStockToListStockDto(stockRepository.findAll());
	}
		
	public void modificarStockBD(Integer referenciaStock, StockDto stockMod) {
		Stock stockEntidad = stockRepository.findByReferenciaPedidoStock(referenciaStock);
		if(stockEntidad == null)
			throw new StockNotFoundException("No se ha encontrado el stock.");
		if(stockMod.getEstadoStock() != null)
			stockEntidad.setEstadoStock(stockMod.getEstadoStock());
		else
			throw new StockNotFoundException("El estado tiene que ser VENDIDO o ALQUILADO.");
		stockRepository.save(stockEntidad);
	}
	
	public void borrarStockBD(Integer referenciaStock) {
		Stock stockEntidad = stockRepository.findByReferenciaPedidoStock(referenciaStock);
		if(stockEntidad == null)
			throw new StockNotFoundException("No se ha encontrado el stock.");
		
		stockRepository.delete(stockEntidad);
	}
}
