package com.formacion.juegoscrud.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.formacion.juegoscrud.conversiones.ConversionJuego;
import com.formacion.juegoscrud.conversiones.ConversionRoles;
import com.formacion.juegoscrud.dto.JuegosDto;
import com.formacion.juegoscrud.dto.RolesDto;
import com.formacion.juegoscrud.entities.Roles;
import com.formacion.juegoscrud.enums.enumUserRoles;
import com.formacion.juegoscrud.repository.RolesRepository;

@Service
public class RolesServiceImpl {

	@Autowired 
	private RolesRepository rolesRepository;
	
	public List<RolesDto> listaRolesDto = new ArrayList<>();

	
	public void addRoles(RolesDto rolesDto) {
	Roles r = new Roles();
	
	r.setRoles(rolesDto.getRoles());
	r.setClientes(rolesDto.getClientes());
	rolesRepository.save(r);
	
	}
	
	public void deleteRoles(enumUserRoles nombre) {
		Roles rolesEntidad = rolesRepository.findByRoles(nombre);
		rolesRepository.delete(rolesEntidad);
	}
	
	public List<RolesDto> mostrarRoles() {
		listaRolesDto.clear();
		listaRolesDto = ConversionRoles.listRolesToListRolesDto(rolesRepository.findAll());
		return listaRolesDto;
	}
}
