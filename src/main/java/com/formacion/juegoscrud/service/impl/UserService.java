package com.formacion.juegoscrud.service.impl;

import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.formacion.juegoscrud.entities.Cliente;
import com.formacion.juegoscrud.entities.Roles;
import com.formacion.juegoscrud.repository.ClienteRepository;

@Service
@Transactional
public class UserService implements UserDetailsService{

	@Autowired
	private ClienteRepository clienteRepository;
	
	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		Cliente c = clienteRepository.findByUsername(username);
		List<GrantedAuthority> roles = new ArrayList<>();
		
		for(Roles rols : c.getRoles())
			roles.add(new SimpleGrantedAuthority(rols.getRoles().toString()));
		
		UserDetails usuario = new User(c.getUsername(), c.getPassword(), roles);
		return usuario;
	}

}
