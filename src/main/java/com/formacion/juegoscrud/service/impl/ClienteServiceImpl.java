package com.formacion.juegoscrud.service.impl;

import java.time.LocalDate;
import java.time.Period;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import com.formacion.juegoscrud.conversiones.ConversionCliente;
import com.formacion.juegoscrud.conversiones.ConversionRoles;
import com.formacion.juegoscrud.dto.ClientesDto;
import com.formacion.juegoscrud.dto.RolesDto;
import com.formacion.juegoscrud.dto.StockDto;
import com.formacion.juegoscrud.entities.Cliente;
import com.formacion.juegoscrud.entities.Roles;
import com.formacion.juegoscrud.entities.Stock;
import com.formacion.juegoscrud.enums.enumUserRoles;
import com.formacion.juegoscrud.exceptions.exceptionscliente.ClienteIMUsedException;
import com.formacion.juegoscrud.exceptions.exceptionscliente.ClienteNotFoundException;
import com.formacion.juegoscrud.repository.ClienteRepository;
import com.formacion.juegoscrud.repository.RolesRepository;
import com.formacion.juegoscrud.repository.StockRepository;
import com.formacion.juegoscrud.service.*;


@Service
public class ClienteServiceImpl implements ClienteService{
	
	@Autowired
	private ClienteRepository clienteRepository;
	
	@Autowired
	private StockRepository stockRepository;
	
	@Autowired 
	private RolesRepository rolesRepository;
	
	@Autowired
	private BCryptPasswordEncoder encoder;
	
	public List<ClientesDto> listaCliente = new ArrayList<>();
	
	/**
	 * SERVICE CRUD BASE DATOS BD
	 */
	
	Logger logger = LoggerFactory.getLogger(ClienteServiceImpl.class); 

	public void addClienteBD( ClientesDto clienteDto) {
		Cliente c = new Cliente();
		Cliente errorDniExiste = clienteRepository.findByDocumentacionCliente(clienteDto.getDocumentacionCliente());
		Cliente errorCorreoExiste = clienteRepository.findByCorreoCliente(clienteDto.getCorreoCliente());
		
		if (errorCorreoExiste != null)
			throw new ClienteIMUsedException("El correo ya existe.");
		if (errorDniExiste != null) {
			logger.error("El dni ya existe logger");
			throw new ClienteIMUsedException("El dni ya existe.");
		}
		
		c.setUsername(clienteDto.getUsername());
		c.setPassword(encoder.encode(clienteDto.getPassword()));
		c.setNombreCliente(clienteDto.getNombreCliente());
		c.setCorreoCliente(clienteDto.getCorreoCliente());
		c.setDocumentacionCliente(clienteDto.getDocumentacionCliente());
		c.setFechaNacimientoCliente(clienteDto.getFechaNacimientoCliente());
		//c.setRoles(ConversionRoles.listRolesDtoToRoles(clienteDto.getRoles()));
		
		List<Roles> listaRolEnt = new ArrayList<Roles>();
		
		for (RolesDto rol : clienteDto.getRoles()) {
			Roles r = rolesRepository.findByRoles(rol.getRoles());
			
			listaRolEnt.add(r);
			r.getClientes().add(c);
		}
		c.setRoles(listaRolEnt);
		clienteRepository.save(c); 

	}
	
	
	public void modificarClienteBD(String dni, ClientesDto clienteMod) {
		Cliente clienteEntidad = clienteRepository.findByDocumentacionCliente(dni);

			if(clienteMod.getUsername() != null)
				clienteEntidad.setUsername(clienteMod.getUsername());
			if(clienteMod.getFechaNacimientoCliente() != null)
				clienteEntidad.setPassword(clienteMod.getPassword());
			if(clienteMod.getNombreCliente() != null)
				clienteEntidad.setNombreCliente(clienteMod.getNombreCliente());
			if(clienteMod.getFechaNacimientoCliente() != null)
				clienteEntidad.setFechaNacimientoCliente(clienteMod.getFechaNacimientoCliente());
			if(clienteMod.getCorreoCliente() != null)
				clienteEntidad.setCorreoCliente(clienteMod.getCorreoCliente());
			if(clienteMod.getDocumentacionCliente() != null)
				clienteEntidad.setDocumentacionCliente(clienteMod.getDocumentacionCliente());
			
			//if(clienteMod.getRoles() != null)
				//clienteEntidad.setRoles(clienteMod.getRoles());
			//clienteMod.setRoles(ConversionRoles.listRolesToListRolesDto(clienteEntidad.getRoles()));
			if(clienteMod.getRoles() != null) {
				List<Roles> listaRolEnt = new ArrayList<Roles>();
				for (RolesDto rol : clienteMod.getRoles()) {
					Roles r = rolesRepository.findByRoles(rol.getRoles());
					listaRolEnt.add(r);
					r.getClientes().add(clienteEntidad);
				}
				clienteEntidad.setRoles(listaRolEnt);
			}
		
				
		clienteRepository.save(clienteEntidad);
	}
	
	/*public void addClienteBD(enumUserRoles rol, ClientesDto clienteDto) {
		Roles rEnt = rolesRepository.findByRoles(rol);
		Cliente c = new Cliente();
		Cliente errorDniExiste = clienteRepository.findByDocumentacionCliente(clienteDto.getDocumentacionCliente());
		Cliente errorCorreoExiste = clienteRepository.findByCorreoCliente(clienteDto.getCorreoCliente());
		
		if (errorCorreoExiste != null)
			throw new ClienteIMUsedException("El correo ya existe.");
		if (errorDniExiste != null) {
			logger.error("El dni ya existe logger");
			throw new ClienteIMUsedException("El dni ya existe.");
		}
		
		c.setUsername(clienteDto.getUsername());
		c.setPassword(encoder.encode(clienteDto.getPassword()));
		c.setNombreCliente(clienteDto.getNombreCliente());
		c.setCorreoCliente(clienteDto.getCorreoCliente());
		c.setDocumentacionCliente(clienteDto.getDocumentacionCliente());
		c.setFechaNacimientoCliente(clienteDto.getFechaNacimientoCliente());
		
		List<Roles> listaRolEnt = new ArrayList<Roles>();
		List<Cliente> listaClieEnt = new ArrayList<Cliente>();
		listaRolEnt.add(rEnt);
		listaClieEnt.add(c);
		
		rEnt.setClientes(listaClieEnt);
		c.setRoles(listaRolEnt);
		
		clienteRepository.save(c);
	}*/

	public ClientesDto getClienteBD(String documentacionCliente) {
		Cliente c = clienteRepository.findByDocumentacionCliente(documentacionCliente);
		
		if (c == null) 
			throw new ClienteNotFoundException("El cliente no existe");
		
		ClientesDto clienteDto = new ClientesDto();
		clienteDto.setUsername(c.getUsername());
		clienteDto.setPassword(c.getPassword());
		clienteDto.setNombreCliente(c.getNombreCliente());
		clienteDto.setCorreoCliente(c.getCorreoCliente());
		clienteDto.setDocumentacionCliente(c.getDocumentacionCliente());
		clienteDto.setFechaNacimientoCliente(c.getFechaNacimientoCliente());
		clienteDto.setRoles(ConversionRoles.listRolesToListRolesDto(c.getRoles()));
		
		return clienteDto;

	}

	

	public void borrarClienteBD(String dni) {
		Cliente clienteEntidad = clienteRepository.findByDocumentacionCliente(dni);
		
		if(clienteEntidad == null)
			throw new ClienteNotFoundException("El cliente no existe");
		
		clienteRepository.delete(clienteEntidad);
	}
	
	public List<ClientesDto> getTodosClientesBD() {
		List<Cliente> listaCliente = clienteRepository.findAll();

		List<ClientesDto> clienteDto = new ArrayList<>();
		for(Cliente c : listaCliente) {
			
			clienteDto.add(ConversionCliente.clienteDtoToCliente(c));
		}		
		return clienteDto;
	}
	
	public static boolean calcularEdad(LocalDate fechaNacimiento, Integer pegi) {
		boolean edadCliente = Period.between(fechaNacimiento, LocalDate.now()).getYears() < pegi;
		return edadCliente;
	}
}
