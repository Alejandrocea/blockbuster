package com.formacion.juegoscrud.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.formacion.juegoscrud.dto.ClientesDto;
import com.formacion.juegoscrud.enums.enumUserRoles;

@Service
public interface ClienteService {
	
	public void addClienteBD(ClientesDto clienteDto);
	
	//public void addClienteBD(enumUserRoles rol, ClientesDto clienteDto);

	
	public ClientesDto getClienteBD(String documentacionCliente);
	
	public void modificarClienteBD(String documentacionCliente, ClientesDto clienteMod);
	
	public void borrarClienteBD(String documentacionCliente);
	
	public List<ClientesDto> getTodosClientesBD();
	
}
