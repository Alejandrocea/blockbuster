package com.formacion.juegoscrud.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.formacion.juegoscrud.dto.StockDto;

@Service
public interface StockService {
	
	public void addStockBD(String tituloJuego, String nombreTienda, String docNumber, StockDto stockDto);
	
	public StockDto getStockPorReferenciaDB(Integer referenciaStock);
	
	public List<StockDto> getTodosStockDB();
	
	public void modificarStockBD(Integer referenciaStock, StockDto stockMod);
	
	public void borrarStockBD(Integer referenciaStock);
	
}
