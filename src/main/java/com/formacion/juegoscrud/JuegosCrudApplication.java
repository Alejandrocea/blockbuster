package com.formacion.juegoscrud;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JuegosCrudApplication {

	public static void main(String[] args) {
		SpringApplication.run(JuegosCrudApplication.class, args);
	}

}
