package com.formacion.juegoscrud.exceptions.exceptiontienda;

import com.formacion.juegoscrud.exceptions.generic.NoContentException;

public class TiendaNoContentException extends NoContentException {

	private static final String DESCRIPTION = "Tienda No Content";

	public TiendaNoContentException(String details) {
		super(DESCRIPTION + ", " + details);
	}
}