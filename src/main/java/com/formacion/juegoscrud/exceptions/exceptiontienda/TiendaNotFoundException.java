package com.formacion.juegoscrud.exceptions.exceptiontienda;

import com.formacion.juegoscrud.exceptions.generic.NotFoundException;

public class TiendaNotFoundException extends NotFoundException {

	private static final String DESCRIPTION = "Store Not Found";

	public TiendaNotFoundException(String details) {
		super(DESCRIPTION + ", " + details);
	}
}
