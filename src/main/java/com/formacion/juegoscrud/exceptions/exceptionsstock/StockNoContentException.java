package com.formacion.juegoscrud.exceptions.exceptionsstock;

import com.formacion.juegoscrud.exceptions.generic.NoContentException;

public class StockNoContentException extends NoContentException {

	private static final String DESCRIPTION = "Stock No Content";

	public StockNoContentException(String details) {
		super(DESCRIPTION + ", " + details);
	}
}
