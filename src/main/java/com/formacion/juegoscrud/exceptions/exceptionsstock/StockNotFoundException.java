package com.formacion.juegoscrud.exceptions.exceptionsstock;

import com.formacion.juegoscrud.exceptions.generic.NotFoundException;

public class StockNotFoundException extends NotFoundException {
	
	private static final String DESCRIPTION = "Stock Not Found";

	public StockNotFoundException(String details) {
		super(DESCRIPTION + ", " + details);
	}
}
