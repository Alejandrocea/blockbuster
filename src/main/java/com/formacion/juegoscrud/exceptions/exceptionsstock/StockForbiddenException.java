package com.formacion.juegoscrud.exceptions.exceptionsstock;

import com.formacion.juegoscrud.exceptions.generic.ForbiddenException;

public class StockForbiddenException extends ForbiddenException {
	
private static final String DESCRIPTION = "Stock Error";
	
	public StockForbiddenException(String details) {
		super(DESCRIPTION + ", " + details);
	}
}
