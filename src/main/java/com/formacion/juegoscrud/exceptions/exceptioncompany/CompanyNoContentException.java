package com.formacion.juegoscrud.exceptions.exceptioncompany;

import com.formacion.juegoscrud.exceptions.generic.NoContentException;

public class CompanyNoContentException extends NoContentException {

	private static final String DESCRIPTION = "Juego No Content";

	public CompanyNoContentException(String details) {
		super(DESCRIPTION + ", " + details);
	}
}
