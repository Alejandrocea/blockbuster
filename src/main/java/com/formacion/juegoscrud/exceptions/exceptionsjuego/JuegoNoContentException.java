package com.formacion.juegoscrud.exceptions.exceptionsjuego;

import com.formacion.juegoscrud.exceptions.generic.NoContentException;

public class JuegoNoContentException extends NoContentException {

	private static final String DESCRIPTION = "Juego No Content";

	public JuegoNoContentException(String details) {
		super(DESCRIPTION + ", " + details);
	}
}
