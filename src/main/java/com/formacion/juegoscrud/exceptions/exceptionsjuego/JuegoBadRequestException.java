package com.formacion.juegoscrud.exceptions.exceptionsjuego;

import com.formacion.juegoscrud.exceptions.generic.BadRequestException;

public class JuegoBadRequestException extends BadRequestException{
	
	private static final String DESCRIPTION = "Juego bad request";
	
	public JuegoBadRequestException(String details) {
		super(DESCRIPTION + ", " + details);
	}
}
