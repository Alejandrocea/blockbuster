package com.formacion.juegoscrud.exceptions.exceptionsjuego;

import com.formacion.juegoscrud.exceptions.generic.NotFoundException;

public class JuegoNotFoundException extends NotFoundException{
	
	private static final String DESCRIPTION = "Game Not Found";

	public JuegoNotFoundException(String details) {
		super(DESCRIPTION + ", " + details);
	}
}

