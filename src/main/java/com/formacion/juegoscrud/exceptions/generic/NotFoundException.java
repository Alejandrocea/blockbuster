package com.formacion.juegoscrud.exceptions.generic;

public class NotFoundException extends RuntimeException {

	private static final String DESCRIPTION = "Not found Exception (404)";
	
	public NotFoundException(String details) {
		super(DESCRIPTION + ", " + details);
	}
}
