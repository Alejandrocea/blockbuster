package com.formacion.juegoscrud.exceptions.generic;

public class IMUsedException extends RuntimeException {

	private static final String DESCRIPTION = "IM Used Exception (226)";

	public IMUsedException(String details) {
		super(DESCRIPTION + ", " + details);
	}
}
