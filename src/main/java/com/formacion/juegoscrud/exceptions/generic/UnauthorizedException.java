package com.formacion.juegoscrud.exceptions.generic;

public class UnauthorizedException extends RuntimeException {
	
	private static final String DESCRIPTION = "Bad Request Exception (400)";
		
		public UnauthorizedException(String details) {
			super(DESCRIPTION + ", " + details);
		}
}
