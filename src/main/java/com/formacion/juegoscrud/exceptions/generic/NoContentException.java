package com.formacion.juegoscrud.exceptions.generic;

public class NoContentException extends RuntimeException {

	private static final String DESCRIPTION = "No content Exception (400)";
	
	public NoContentException(String details) {
		super(DESCRIPTION + ", " + details);
	}
	//TODO cambiar todos los not found por no content
}
