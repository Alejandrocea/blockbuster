package com.formacion.juegoscrud.exceptions.generic;

public class ForbiddenException extends RuntimeException {
	
private static final String DESCRIPTION = "Forbidden (403)";
	
	public ForbiddenException(String details) {
		super(DESCRIPTION + ", " + details);
	}
}
