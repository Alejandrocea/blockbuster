package com.formacion.juegoscrud.exceptions.exceptionscliente;

import com.formacion.juegoscrud.exceptions.generic.NoContentException;

public class ClienteNoContentException extends NoContentException {

	private static final String DESCRIPTION = "Cliente No Content";

	public ClienteNoContentException(String details) {
		super(DESCRIPTION + ", " + details);
	}
}