package com.formacion.juegoscrud.exceptions.exceptionscliente;

import com.formacion.juegoscrud.exceptions.generic.NotFoundException;

public class ClienteNotFoundException extends NotFoundException {

	private static final String DESCRIPTION = "Client Not Found";

	public ClienteNotFoundException(String details) {
		super(DESCRIPTION + ", " + details);
	}
}
