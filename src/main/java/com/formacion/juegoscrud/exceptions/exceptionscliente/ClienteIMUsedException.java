package com.formacion.juegoscrud.exceptions.exceptionscliente;

import com.formacion.juegoscrud.exceptions.generic.IMUsedException;

public class ClienteIMUsedException extends IMUsedException {
	
	private static final String DESCRIPTION = "Client IM Used";
 
	public ClienteIMUsedException(String details) {
		super(DESCRIPTION + ", " + details);
	}
}
