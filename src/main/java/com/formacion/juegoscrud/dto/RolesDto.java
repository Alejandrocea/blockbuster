package com.formacion.juegoscrud.dto;

import java.util.List;

import com.formacion.juegoscrud.entities.Cliente;
import com.formacion.juegoscrud.enums.enumUserRoles;

import lombok.Data;

@Data
public class RolesDto {

	private enumUserRoles roles;
	
	private List<Cliente> clientes;

	
}
