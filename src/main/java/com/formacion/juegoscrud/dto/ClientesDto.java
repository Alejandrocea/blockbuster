package com.formacion.juegoscrud.dto;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;

import org.springframework.format.annotation.DateTimeFormat;

import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.Data;

@Data
public class ClientesDto {
	
	private Long id;
	
	@NotBlank(message = "El nombre de usuario no puede estar vacío")
	private String username;
	
	@NotBlank(message = "El password no puede estar vacío")
	private String password;
	
	private List<RolesDto> roles = new ArrayList();
	//private enumUserRoles roles;
	
	@NotBlank(message = "El nombre no puede estar vacío")
	private String nombreCliente;
	
	@DateTimeFormat
	//@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd/MM/yyyy", timezone = "Europe/London") 
	private LocalDate fechaNacimientoCliente;
	
	@NotBlank(message = "El correo no puede estar vacío")
	@Email
	private String correoCliente;
	
	@NotBlank(message = "El documento no puede estar vacío")
	private String documentacionCliente;
}
