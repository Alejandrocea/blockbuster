package com.formacion.juegoscrud.dto;

import java.util.List;

import javax.validation.constraints.NotBlank;

import lombok.Data;

@Data
public class CompanyDto {
	
	@NotBlank(message = "cif de la compañía vacío")
	private String cifCompany;
	
	@NotBlank(message = "El nombre de la compñía no puede estar vacío")
	private String nombreCompany;
	
	private List<JuegosDto> Juegos;

}
