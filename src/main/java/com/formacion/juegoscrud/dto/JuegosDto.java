package com.formacion.juegoscrud.dto;

import java.util.Date;
import java.util.List;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.formacion.juegoscrud.enums.enumCategoriaJuego;

import lombok.Data;

@Data
public class JuegosDto {
	
	@NotBlank(message = "Titulo del juego no puede estar vacío")
	private String titulo;
	
	//@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd/MM/yyyy", timezone = "Europe/London") 
	private Date fechaLanzamientoJuego;
	
	@NotNull(message = "La categoría del juego no puede estar vacía, elegir entre ARCADE,AVENTURA,SHOOTER o CARRERAS")
	private enumCategoriaJuego categoriasJuegos;
	
	@NotNull(message = "El juego debe tener algún pegi")
	@Positive
	private Integer pegiEdadJuego;

	//private CompanyDto companies;
	private List<CompanyDto> company;	
}


