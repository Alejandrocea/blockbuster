package com.formacion.juegoscrud.dto;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;

import com.formacion.juegoscrud.enums.enumEstadoJuego;

import lombok.Data;

@Data
public class StockDto {

	@NotNull(message = "Hay que añadirle una referencia al stock")
	@Positive
	private Integer referenciaPedidoStock;
	
	@NotNull(message = "El estado tiene que ser VENDIDO o ALQUILADO")
	private enumEstadoJuego estadoStock;
	
	private Integer referenciaJuegoStock;
	
	public TiendaDto tienda;
	public JuegosDto juegos;
	public ClientesDto clientes;
	
}
