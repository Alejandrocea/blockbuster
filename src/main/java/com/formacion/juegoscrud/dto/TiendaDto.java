package com.formacion.juegoscrud.dto;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;

import lombok.Data;

@Data
public class TiendaDto {

	@NotBlank(message = "El nombre de la tienda puede estar vacío")
	private String nombreTienda;
	@NotNull(message = "El numero de la tienda no puede estar vacío")
	@Positive
	private Integer numeroTienda;
}
