package com.formacion.juegoscrud.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.formacion.juegoscrud.dto.ClientesDto;
import com.formacion.juegoscrud.enums.enumUserRoles;
import com.formacion.juegoscrud.service.ClienteService;

@RestController
@CrossOrigin
public class ClienteController {

	@Autowired
	private ClienteService clienteService;
	
	/**
	 * CONTROLLER CRUD BASE DATOS BD
	 */

	@GetMapping("/cliente")
	public ResponseEntity<List<ClientesDto>> getTodosClienteDB(){
		return new ResponseEntity<List<ClientesDto>>(clienteService.getTodosClientesBD(), HttpStatus.OK);
	}
	
	/**
	 * Con esta funcion agregarClienteBD se le pasa por postman en formato Json los datos del ClienteDto
	 * y agrega el cliente a la base de datos. 
	 */
	/*@PostMapping("/agregarClienteBD")
	public void agregarClienteBD(@RequestParam enumUserRoles rol, @Valid @RequestBody ClientesDto clienteDto) {
		clienteService.addClienteBD(rol, clienteDto);
	}*/
	
	@PostMapping("/cliente/")
	public void agregarClienteBD(@RequestBody ClientesDto clienteDto) {
		clienteService.addClienteBD( clienteDto);
	}
	
	/*@GetMapping("/buscarClientePorDniBD")
	public ResponseEntity<ClientesDto> getClienteDniBD(@RequestParam String dniCliente){				
		ClientesDto cliente = clienteService.getClienteBD(dniCliente);
		return new ResponseEntity<>(cliente, HttpStatus.OK);
	}*/
	
	@GetMapping("/cliente/{documentacionCliente}")
	public ResponseEntity<ClientesDto> getClienteDniBD(@PathVariable String documentacionCliente){				
		ClientesDto cliente = clienteService.getClienteBD(documentacionCliente);
		return new ResponseEntity<>(cliente, HttpStatus.OK);
	}
	
	/*@PostMapping("/modificarClienteBD")
	public void modificarClienteControllerBD(@RequestHeader String dniCliente, @RequestBody ClientesDto clienteMod) {
		clienteService.modificarClienteBD(dniCliente, clienteMod);
	}*/
	
	@PutMapping("/cliente/{documentacionCliente}")
	public void modificarClienteControllerBD(@PathVariable String documentacionCliente,  @RequestBody ClientesDto clienteMod) {
		clienteService.modificarClienteBD(documentacionCliente, clienteMod);
	}
	
	/*@GetMapping("/borrarClientePorDniBD")
	public void borrarClientePorDniBD(@RequestParam String dni) {
		clienteService.borrarClienteBD(dni);
	}	*/
	
	@DeleteMapping("/cliente/{documentacionCliente}")
	public void borrarClientePorDniBD(@PathVariable String documentacionCliente) {
		clienteService.borrarClienteBD(documentacionCliente);
	}	
	
	
}
