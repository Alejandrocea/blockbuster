package com.formacion.juegoscrud.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.formacion.juegoscrud.dto.ClientesDto;
import com.formacion.juegoscrud.dto.CompanyDto;
import com.formacion.juegoscrud.dto.JuegosDto;
import com.formacion.juegoscrud.dto.TiendaDto;
import com.formacion.juegoscrud.service.impl.JuegosCompanyTiendaServiceImpl;

@RestController
@CrossOrigin(origins = "*")
public class JuegosCompanyTiendaController {
	
	@Autowired
	private JuegosCompanyTiendaServiceImpl juegosCompanyTiendaService;
	
	
	/**
	 * JUEGOS
	 */
	@PostMapping("/insertarJsonJuegos")
	public void insertarJuegos() {
		//juegosCompanyTiendaService.agregarJuegosJson();
	}	
	
	@PostMapping("/juego")
	public void añadirJuego(@RequestBody JuegosDto juegosdto) {
		juegosCompanyTiendaService.añadirJuego(juegosdto);
	}
	
	@GetMapping("/juego")
	public ResponseEntity<List<JuegosDto>> getJuegosBD() {
		return new ResponseEntity<List<JuegosDto>>(juegosCompanyTiendaService.getTodosJuegosBD(), HttpStatus.OK);
	}
	
	@GetMapping("/juego/{titulo}")
	public ResponseEntity<JuegosDto> getUnJuegoBD(@PathVariable String titulo) {
		return new ResponseEntity<JuegosDto>(juegosCompanyTiendaService.getUnJuegoBD(titulo), HttpStatus.OK);
	}
	
	@PutMapping("/juego/{titulo}")
	public void modificarJuegoBD(@PathVariable String titulo,  @RequestBody JuegosDto juegoMod) {
		juegosCompanyTiendaService.modificarJuego(titulo, juegoMod);
	}
	
	@DeleteMapping("/juego/{titulo}")
	public void borrarClientePorDniBD(@PathVariable String titulo) {
		juegosCompanyTiendaService.borrarJuego(titulo);
	}	
	
	
	/**
	 * TIENDAS
	 */
	@PostMapping("/tienda")
	public void insertarTienda() {
		juegosCompanyTiendaService.agregarTiendaJson();
	}
	
	@GetMapping("/tienda")
	public ResponseEntity<List<TiendaDto>> getTiendas() {
		return new ResponseEntity<List<TiendaDto>>(juegosCompanyTiendaService.getTodosTienda(), HttpStatus.OK);
	}
	
	
	
	/**
	 * COMPANIES
	 */
	@PostMapping("/insertarJsonCompany")
	public void insertarCompany() {
		//juegosCompanyTiendaService.agregarCompanyJson();
	}
	
	@PostMapping("/company")
	public void añadirCompañia(@RequestBody CompanyDto compañiadto) {
		juegosCompanyTiendaService.añadirCompañia(compañiadto);
	}
	
	@GetMapping("/company")
	public ResponseEntity<List<CompanyDto>> getCompanies() {
		return new ResponseEntity<List<CompanyDto>>(juegosCompanyTiendaService.getTodosCompany(), HttpStatus.OK);
	}
	
	@GetMapping("/company/{titulo}")
	public ResponseEntity<CompanyDto> getUnaCompanyBD(@PathVariable String titulo) {
		return new ResponseEntity<CompanyDto>(juegosCompanyTiendaService.getUnaCompanyBD(titulo), HttpStatus.OK);
	}
	
	@DeleteMapping("/company/{companyName}")
	public void borrarCompanyPorNombreBD(@PathVariable String companyName) {
		juegosCompanyTiendaService.borrarCompany(companyName);
	}	
}
