package com.formacion.juegoscrud.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.formacion.juegoscrud.dto.StockDto;
import com.formacion.juegoscrud.service.impl.StockServiceImpl;

@RestController
@CrossOrigin(origins = "*")
public class StockController {

	@Autowired
	private StockServiceImpl stockService;

	/**
	 * CONTROLLER CRUD BASE DATOS BD
	 */
	
	/**
	 * Este método crea un stock/pedido, que se relaciona con las entidades cliente, tienda, y juegos
	 * @param tituloJuego el nombre del juego que queramos relacionar con stock
	 * @param nombreTienda nombre de la tienda que queramos relacionar con stock
	 * @param documento documento de identidad del cliente que queremos relacionar con el stock
	 * @param stockDto el objeto en formato Json donde le pasaremos la referencia del pedido, y el stado del stock: ALQUILADO/VENDIDO
	 */
	/*@PostMapping("/addStockBD")
	public void addStockController(@RequestHeader String tituloJuego, @RequestHeader String nombreTienda, @RequestHeader String documento, @Valid @RequestBody StockDto stockDto) {
		stockService.addStockBD(tituloJuego, nombreTienda, documento, stockDto);
	}*/
	
	@PostMapping("/stock")
	public void addStockController(@RequestHeader String tituloJuego, @RequestHeader String nombreTienda, @RequestHeader String documento, @Valid @RequestBody StockDto stockDto) {
		stockService.addStockBD(tituloJuego, nombreTienda, documento, stockDto);
	}
	
	@GetMapping("/stock/{referenciaStock}")
	public ResponseEntity<StockDto> getStockReferenciaDB(@PathVariable Integer referenciaStock){
		StockDto stock = stockService.getStockPorReferenciaDB(referenciaStock);
		return new ResponseEntity<>(stock, HttpStatus.OK);
	}
	
	@PostMapping("/stock/{referenciaStock}")
	public void modificarStockControllerBD(@PathVariable Integer referenciaStock, @RequestBody StockDto stockMod) {
		stockService.modificarStockBD(referenciaStock, stockMod);
	}
	
	@DeleteMapping("/stock/{referenciaStock}")
	public void borrarStockPorReferenciaBD(@PathVariable Integer referenciaStock) {
		stockService.borrarStockBD(referenciaStock);
	}
	
	@GetMapping("/stock")
	public ResponseEntity<List<StockDto>> getTodosStockDB(){
		return new ResponseEntity<List<StockDto>>(stockService.getTodosStockDB(), HttpStatus.OK);
	}
}
