package com.formacion.juegoscrud.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.formacion.juegoscrud.dto.RolesDto;
import com.formacion.juegoscrud.enums.enumUserRoles;
import com.formacion.juegoscrud.service.impl.RolesServiceImpl;

@RestController
@CrossOrigin(origins = "*")
public class RolesController {

	@Autowired 
	private RolesServiceImpl rolesService;
	
	@PostMapping("/roles")
	public void agregarRol(@RequestBody RolesDto rolesDto) {
		rolesService.addRoles(rolesDto);
	}
	
	@DeleteMapping("/roles/{enumUserRoles}")
	public void eliminarRol(@PathVariable enumUserRoles rol) {
		rolesService.deleteRoles(rol);
	}
	
	@GetMapping("/roles")
	public ResponseEntity<List<RolesDto>> getTodosRolesDB(){
		return new ResponseEntity<List<RolesDto>>(rolesService.mostrarRoles(), HttpStatus.OK);
	}
}

