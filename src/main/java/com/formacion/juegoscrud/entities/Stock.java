package com.formacion.juegoscrud.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.formacion.juegoscrud.enums.enumEstadoJuego;

import lombok.Data;

@Entity
@Table(name = "STOCK")
@Data
public class Stock {
	
	@Id
	@Column(name = "ID_PEDIDO")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long idPedido;
	
	@Column(name = "REFERENCIA_STOCK")
	private Integer referenciaPedidoStock;
	
	@Enumerated(EnumType.STRING)
	private enumEstadoJuego estadoStock;
	
//	@Column(name = "REFERENCIA_JUEGO")
//	private Integer referenciaJuegoStock;

	@ManyToOne
	private Cliente cliente;
	
	@ManyToOne
	private Juegos juegos;
	
	@ManyToOne
	private Tienda tiendaObj;
	
	
	
	//en juegos hay que poner la compañia, y luego tienda con los datos del juego

}
