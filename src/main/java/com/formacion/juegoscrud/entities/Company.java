package com.formacion.juegoscrud.entities;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Table(name = "COMPANY")
@Data
public class Company {
	@Id
	@Column(name = "ID_COMPANY")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long idCompany;
	
	@Column(name = "CIF_COMPANY", unique = true)
	private String cifCompany;
	
	@Column(name = "NOMBRE_COMPANY")
	private String nombreCompany;

	@ManyToMany(cascade = CascadeType.ALL, mappedBy = "company")
	private List<Juegos> juegos = new ArrayList<Juegos>();

}
