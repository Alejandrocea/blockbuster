package com.formacion.juegoscrud.entities;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import lombok.Data;

@Entity //Para decirle a Spring que esto es una entidad
@Table(name = "CLIENTE") // se le asigna el nombre de la tabla
@Data //Sustituye los getter y setters, no hace falta declararlos
public class Cliente {
	
	@Id //Para decirle que es una PrimaryKey
	@Column(name = "ID_CLIENTE")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long idCliente;
	
	@Column(unique = true, name = "NOMBRE_USUARIO")
	private String username;
	
	@Column(name = "CONTRASEÑA")
	private String password;
	
	@Column(name = "NOMBRE_CLIENT")
	private String nombreCliente;
	
	@Column(name = "FECHANACIMIENTO")
	private LocalDate fechaNacimientoCliente;
	
	@Column(name = "CORREO")
	private String correoCliente;
	
	@Column(name = "DOCUMENTO")
	private String documentacionCliente;	

	@OneToMany(cascade = CascadeType.ALL, mappedBy = "cliente")
	private List<Stock> stocks = new ArrayList<Stock>();
	
	@ManyToMany
	private List<Roles> roles = new ArrayList<Roles>();

}
