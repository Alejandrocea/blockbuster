package com.formacion.juegoscrud.entities;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Table(name = "TIENDA")
@Data
public class Tienda {
	
	@Id
	@Column(name = "ID_TIENDA")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long idTienda;
	
	@Column(name = "NOMBRE_TIENDA")
	private String nombreTienda;
	
	@Column(name = "STOCK_TIENDA")
	private Integer stockTienda;
	
	@OneToMany(cascade = CascadeType.ALL, mappedBy = "tiendaObj")
	private List<Stock> stocks = new ArrayList<Stock>();
}
