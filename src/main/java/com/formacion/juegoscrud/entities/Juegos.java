package com.formacion.juegoscrud.entities;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.formacion.juegoscrud.enums.enumCategoriaJuego;

import lombok.Data;

@Entity
@Table(name = "JUEGOS")
@Data
public class Juegos {

	@Id
	@Column(name = "ID_JUEGO")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long idJuego;
	
	@Column(name = "TITULO")
	private String titulo;
	
	@Column(name = "FECHA_LANZAMIENTO")
	private Date fechaLanzamientoJuego;
	
	//@Column(name = "CATEGORIA_JUEGO")
	@Enumerated(EnumType.STRING)
	private enumCategoriaJuego categoriasJuegos;

	@Column(name = "PEGI")
	private Integer pegiEdadJuego;
	
	@OneToMany(cascade = CascadeType.ALL, mappedBy = "juegos")
	private List<Stock> stocks = new ArrayList<Stock>();
	
	@ManyToMany
	private List<Company> company = new ArrayList<Company>();
}
