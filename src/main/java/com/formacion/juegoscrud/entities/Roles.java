package com.formacion.juegoscrud.entities;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import com.formacion.juegoscrud.enums.enumUserRoles;

import lombok.Data;

@Entity
@Table(name = "ROLES")
@Data
public class Roles {

	@Id
	@Column(name = "ID_ROL")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long idRol;
	
	@Enumerated(EnumType.STRING)
	private enumUserRoles roles;
	
	@ManyToMany(cascade = CascadeType.ALL, mappedBy = "roles")
	private List<Cliente> clientes = new ArrayList<Cliente>();
}
