package com.formacion.juegoscrud.conversiones;

import java.util.ArrayList;
import java.util.List;

import com.formacion.juegoscrud.dto.RolesDto;
import com.formacion.juegoscrud.entities.Roles;

public class ConversionRoles {

	public static List<RolesDto> listRolesToListRolesDto (List<Roles> listaRolesEnt){
		List<RolesDto> listaRolesDto = new ArrayList<RolesDto>();
		for(Roles rolesEnt : listaRolesEnt) {
			RolesDto rolesDto = new RolesDto();
			
			rolesDto.setRoles(rolesEnt.getRoles());
			//rolesDto.setClientes(rolesEnt.getClientes());
			
			listaRolesDto.add(rolesDto);
		}
		return listaRolesDto;
	}

	public static List<Roles> listRolesDtoToRoles (List<RolesDto> listaRolesDto){
		List<Roles> listaRolesEnt = new ArrayList<Roles>();
		for(RolesDto rolesDto : listaRolesDto) {
			Roles rolesEnt = new Roles();
			
			rolesEnt.setRoles(rolesDto.getRoles());
			//rolesEnt.setClientes(rolesDto.getClientes());
			
			listaRolesEnt.add(rolesEnt);
		}
		return listaRolesEnt;
	}
}
