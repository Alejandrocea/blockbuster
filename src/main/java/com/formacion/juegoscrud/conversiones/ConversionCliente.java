package com.formacion.juegoscrud.conversiones;

import java.util.ArrayList;
import java.util.List;

import com.formacion.juegoscrud.dto.ClientesDto;
import com.formacion.juegoscrud.entities.Cliente;


public class ConversionCliente {
	
	public static Cliente clienteToClienteDto (ClientesDto clienteDto)  {
		
		Cliente clienteEnt = new Cliente();
		
		clienteEnt.setNombreCliente(clienteDto.getNombreCliente());
		clienteEnt.setCorreoCliente(clienteDto.getCorreoCliente());
		clienteEnt.setDocumentacionCliente(clienteDto.getDocumentacionCliente());
		clienteEnt.setFechaNacimientoCliente(clienteDto.getFechaNacimientoCliente());
		
		return clienteEnt;
	}
	
	public static ClientesDto clienteDtoToCliente (Cliente clienteEnt)  {
		
		ClientesDto clienteDto = new ClientesDto();
		clienteDto.setId(clienteEnt.getIdCliente());
		clienteDto.setUsername(clienteEnt.getUsername());
		clienteDto.setPassword(clienteEnt.getPassword());
		//clienteDto.setRoles(clienteEnt.getRol());
		clienteDto.setNombreCliente(clienteEnt.getNombreCliente());
		clienteDto.setCorreoCliente(clienteEnt.getCorreoCliente());
		clienteDto.setDocumentacionCliente(clienteEnt.getDocumentacionCliente());
		clienteDto.setFechaNacimientoCliente(clienteEnt.getFechaNacimientoCliente());
		clienteDto.setRoles(ConversionRoles.listRolesToListRolesDto(clienteEnt.getRoles()));
		
		return clienteDto;
	}
	
	public static List<ClientesDto> listClienteToListClienteDto (List<Cliente> listaClienteEnt) {		
		 List<ClientesDto> listClienteDto = new ArrayList<ClientesDto>();
		for(Cliente clienteEnt : listaClienteEnt) {
			ClientesDto clienteDto = new ClientesDto();
			
			clienteDto.setNombreCliente(clienteEnt.getNombreCliente());
			clienteDto.setCorreoCliente(clienteEnt.getCorreoCliente());
			clienteDto.setFechaNacimientoCliente(clienteEnt.getFechaNacimientoCliente());
			clienteDto.setDocumentacionCliente(clienteEnt.getDocumentacionCliente());
			listClienteDto.add(clienteDto);			
		}		
		return listClienteDto;
	}
	
	public static List<Cliente> listClienteDtoToListCliente (List<ClientesDto> listaClienteDto) {		
		 List<Cliente> listClienteEnt = new ArrayList<Cliente>();
		for(ClientesDto clienteDto : listaClienteDto) {
			Cliente clienteEnt = new Cliente();
			
			clienteEnt.setNombreCliente(clienteDto.getNombreCliente());
			clienteEnt.setCorreoCliente(clienteDto.getCorreoCliente());
			clienteEnt.setFechaNacimientoCliente(clienteDto.getFechaNacimientoCliente());
			clienteEnt.setDocumentacionCliente(clienteDto.getDocumentacionCliente());
			listClienteEnt.add(clienteEnt);			
		}		
		return listClienteEnt;
	}

}
