package com.formacion.juegoscrud.conversiones;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Repository;

import com.formacion.juegoscrud.dto.TiendaDto;
import com.formacion.juegoscrud.entities.Tienda;


@Repository
public class ConversionTienda {
	
	public static Tienda tiendaDtoTotienda (TiendaDto tiendaDto)  {
		
		Tienda tiendaEnt = new Tienda();
		
		tiendaEnt.setStockTienda(tiendaDto.getNumeroTienda());
		tiendaEnt.setNombreTienda(tiendaDto.getNombreTienda());
				
		return tiendaEnt;
	}
	
	public static TiendaDto tiendaTotiendaDto (Tienda tiendaEnt)  {
		
		TiendaDto tiendaDto = new TiendaDto();
		
		tiendaDto.setNumeroTienda(tiendaEnt.getStockTienda());
		tiendaDto.setNombreTienda(tiendaEnt.getNombreTienda());
				
		return tiendaDto;
	}
	
	public static List<TiendaDto> listStockToListStockDto (List<Tienda> listaTiendaEnt) {		
		List<TiendaDto> listTiendaDto = new ArrayList<TiendaDto>();
		for(Tienda tiendaEnt : listaTiendaEnt) {
			TiendaDto tiendaDto = new TiendaDto();
			
			tiendaDto.setNumeroTienda(tiendaEnt.getStockTienda());
			tiendaDto.setNombreTienda(tiendaEnt.getNombreTienda());
			listTiendaDto.add(tiendaDto);		
		}		
		return listTiendaDto;
	}
	
	public static List<Tienda> listStockDtoToListStock (List<TiendaDto> listaTiendaDto) {		
		List<Tienda> listTiendaEnt = new ArrayList<Tienda>();
		for(TiendaDto tiendaDto : listaTiendaDto) {
			Tienda tiendaEnt = new Tienda();
			
			tiendaEnt.setStockTienda(tiendaDto.getNumeroTienda());
			tiendaEnt.setNombreTienda(tiendaDto.getNombreTienda());
			listTiendaEnt.add(tiendaEnt);		
		}		
		return listTiendaEnt;
	}
	
}
