package com.formacion.juegoscrud.conversiones;

import java.util.ArrayList;
import java.util.List;

import com.formacion.juegoscrud.dto.JuegosDto;
import com.formacion.juegoscrud.entities.Juegos;


public class ConversionJuego {
		
	public static Juegos juegoDtoToJuego (JuegosDto juegoDto)  {
		
		Juegos juegoEnt = new Juegos();
		
		juegoEnt.setTitulo(juegoDto.getTitulo());
		juegoEnt.setCategoriasJuegos(juegoDto.getCategoriasJuegos());
		juegoEnt.setFechaLanzamientoJuego(juegoDto.getFechaLanzamientoJuego());
		juegoEnt.setPegiEdadJuego(juegoDto.getPegiEdadJuego());
		
		return juegoEnt;
	}
	
	public static JuegosDto juegoToJuegoDto (Juegos juegoEnt)  {
		
		JuegosDto juegoDto = new JuegosDto();
		
		juegoDto.setTitulo(juegoEnt.getTitulo());
		juegoDto.setCategoriasJuegos(juegoEnt.getCategoriasJuegos());
		juegoDto.setFechaLanzamientoJuego(juegoEnt.getFechaLanzamientoJuego());
		juegoDto.setPegiEdadJuego(juegoEnt.getPegiEdadJuego());
		juegoDto.setCompany(ConversionCompañia.listCompañiaToListCompañiaDto(juegoEnt.getCompany()));
		return juegoDto;
	}
	
	public static List<JuegosDto> listJuegoToListJuegoDto (List<Juegos> listaJuegoEnt) {		
		List<JuegosDto> listJuegoDto = new ArrayList<JuegosDto>();
		for(Juegos juegoEnt : listaJuegoEnt) {
			JuegosDto juegoDto = new JuegosDto();
			
			juegoDto.setTitulo(juegoEnt.getTitulo());
			juegoDto.setCategoriasJuegos(juegoEnt.getCategoriasJuegos());
			juegoDto.setFechaLanzamientoJuego(juegoEnt.getFechaLanzamientoJuego());
			juegoDto.setPegiEdadJuego(juegoEnt.getPegiEdadJuego());
			juegoDto.setCompany(ConversionCompañia.listCompañiaToListCompañiaDto(juegoEnt.getCompany()));
			listJuegoDto.add(juegoDto);		
		}		
		return listJuegoDto;
	}
	
	public static List<Juegos> listJuegoDtoToListJuegoD (List<JuegosDto> listaJuegoDto) {		
		List<Juegos> listJuegoEnt = new ArrayList<Juegos>();
		for(JuegosDto juegoDto : listaJuegoDto) {
			Juegos juegoEnt = new Juegos();
			
			juegoEnt.setTitulo(juegoDto.getTitulo());
			juegoEnt.setCategoriasJuegos(juegoDto.getCategoriasJuegos());
			juegoEnt.setFechaLanzamientoJuego(juegoDto.getFechaLanzamientoJuego());
			juegoEnt.setPegiEdadJuego(juegoDto.getPegiEdadJuego());
			listJuegoEnt.add(juegoEnt);		
		}		
		return listJuegoEnt;
	}
}

