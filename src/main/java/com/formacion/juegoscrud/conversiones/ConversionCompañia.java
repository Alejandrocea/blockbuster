package com.formacion.juegoscrud.conversiones;

import java.util.ArrayList;
import java.util.List;

import com.formacion.juegoscrud.dto.CompanyDto;
import com.formacion.juegoscrud.entities.Company;


public class ConversionCompañia {
	
	public static Company compañiaDtoToCompañia (CompanyDto compañiaDto)  {
		
		Company compañiaEnt = new Company();
		
		compañiaEnt.setNombreCompany(compañiaDto.getNombreCompany());
		compañiaEnt.setCifCompany(compañiaDto.getCifCompany());
	
		return compañiaEnt;
	}
	
	public static CompanyDto compañiaToCompañiaDto (Company compañiaEnt)  {
		
		CompanyDto compañiaDto = new CompanyDto();
		
		compañiaDto.setNombreCompany(compañiaEnt.getNombreCompany());
		compañiaDto.setCifCompany(compañiaEnt.getCifCompany());
		compañiaDto.setJuegos(ConversionJuego.listJuegoToListJuegoDto(compañiaEnt.getJuegos()));
		return compañiaDto;
	}
	
	public static List<CompanyDto> listCompañiaToListCompañiaDto (List<Company> listaCompañiaEnt) {		
		List<CompanyDto> listCompañiaDto = new ArrayList<CompanyDto>();

		for(Company compañiaEnt : listaCompañiaEnt) {
			CompanyDto compañiaDto = new CompanyDto();
			
			compañiaDto.setNombreCompany(compañiaEnt.getNombreCompany());
			compañiaDto.setCifCompany(compañiaEnt.getCifCompany());
			//compañiaDto.setJuegos(ConversionJuego.listJuegoToListJuegoDto(compañiaEnt.getJuegos()));
			listCompañiaDto.add(compañiaDto);		
		}		
		return listCompañiaDto;
	}
	
	public static List<Company> listCompañiaDtoToListCompañia (List<CompanyDto> listaCompañiaDto) {		
		List<Company> listCompañiaEnt = new ArrayList<Company>();
		for(CompanyDto compañiaDto : listaCompañiaDto) {
			Company compañiaEnt = new Company();
			
			compañiaEnt.setNombreCompany(compañiaDto.getNombreCompany());
			compañiaEnt.setCifCompany(compañiaDto.getCifCompany());
		
			listCompañiaEnt.add(compañiaEnt);		
		}		
		return listCompañiaEnt;
	}

	
}
