package com.formacion.juegoscrud.conversiones;

import java.util.ArrayList;
import java.util.List;

import com.formacion.juegoscrud.dto.StockDto;
import com.formacion.juegoscrud.entities.Stock;


public class ConverisionStock {
		
	public static Stock stockDtoToStock (StockDto stockDto)  {
		
		Stock stockEnt = new Stock();
		
		stockEnt.setEstadoStock(stockDto.getEstadoStock());
		stockEnt.setReferenciaPedidoStock(stockDto.getReferenciaPedidoStock());
		
		return stockEnt;
	}
	
	public static StockDto stockToStockDto (Stock stockEnt)  {
		
		StockDto stockDto = new StockDto();
		
		stockDto.setEstadoStock(stockEnt.getEstadoStock());
		stockDto.setReferenciaPedidoStock(stockEnt.getReferenciaPedidoStock());
		stockDto.setJuegos(ConversionJuego.juegoToJuegoDto(stockEnt.getJuegos()));
		stockDto.setTienda(ConversionTienda.tiendaTotiendaDto(stockEnt.getTiendaObj()));
		stockDto.setClientes(ConversionCliente.clienteDtoToCliente(stockEnt.getCliente()));
		
		return stockDto;
	}
	
	public static List<StockDto> listStockToListStockDto (List<Stock> listaStockEnt) {		
		List<StockDto> listStockDto = new ArrayList<StockDto>();
		for(Stock stockEnt : listaStockEnt) {
			StockDto stockDto = new StockDto();
			
			stockDto.setEstadoStock(stockEnt.getEstadoStock());
			stockDto.setReferenciaPedidoStock(stockEnt.getReferenciaPedidoStock());
			
			listStockDto.add(stockDto);		
		}		
		return listStockDto;
	}
	
	public static List<Stock> listStockDtoToListStock (List<StockDto> listaStockDto) {		
		List<Stock> listStockEnt = new ArrayList<Stock>();
		for(StockDto stockDto : listaStockDto) {
			Stock stockEnt = new Stock();
			
			stockEnt.setEstadoStock(stockDto.getEstadoStock());
			stockEnt.setReferenciaPedidoStock(stockDto.getReferenciaPedidoStock());
			
			listStockEnt.add(stockEnt);		
		}		
		return listStockEnt;
	}

}
