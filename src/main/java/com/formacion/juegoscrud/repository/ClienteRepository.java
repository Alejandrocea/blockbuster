package com.formacion.juegoscrud.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.formacion.juegoscrud.entities.Cliente;

@Repository //en estas clases interactuamos con las tablas de la base de datos
public interface ClienteRepository extends JpaRepository<Cliente, Long>{ //coge la la tabla y la PK
	public Cliente findByDocumentacionCliente(String documentacionCliente);
	public Cliente findByCorreoCliente(String correoCliente);
	public Cliente findByUsername(String username);
	//Optional<Cliente> findByDocumentacionCliente(String documentacionCliente);

	
}
