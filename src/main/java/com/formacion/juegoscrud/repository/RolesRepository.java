package com.formacion.juegoscrud.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.formacion.juegoscrud.dto.RolesDto;
import com.formacion.juegoscrud.entities.Roles;
import com.formacion.juegoscrud.enums.enumUserRoles;

@Repository
public interface RolesRepository extends JpaRepository<Roles, Long>{
	public Roles findByRoles(enumUserRoles roles);


	
}
