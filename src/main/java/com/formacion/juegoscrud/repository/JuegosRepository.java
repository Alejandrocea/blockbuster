package com.formacion.juegoscrud.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.formacion.juegoscrud.entities.Company;
import com.formacion.juegoscrud.entities.Juegos;

@Repository
public interface JuegosRepository  extends JpaRepository<Juegos, Long>{
	public Juegos findByTitulo(String titulo);
	
}
