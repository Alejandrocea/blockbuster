package com.formacion.juegoscrud.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.formacion.juegoscrud.entities.Tienda;

@Repository
public interface TiendaRepository  extends JpaRepository<Tienda, Long>{
	public Tienda findByNombreTienda(String nombreTienda);
	
}
