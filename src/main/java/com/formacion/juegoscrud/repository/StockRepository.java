package com.formacion.juegoscrud.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.formacion.juegoscrud.entities.Cliente;
import com.formacion.juegoscrud.entities.Stock;

public interface StockRepository extends JpaRepository<Stock, Long>{
	public Stock findByReferenciaPedidoStock(Integer referenciaPedidoStock);
	
	public List<Stock> findByCliente(Cliente cliente);
	
}
