package com.formacion.juegoscrud.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.formacion.juegoscrud.entities.Company;

@Repository
public interface CompanyRepository  extends JpaRepository<Company, Long>{
	public Company findByNombreCompany(String companyName);
	
}
