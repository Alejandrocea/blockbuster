package com.formacion.juegoscrud;

import java.util.Arrays;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.CorsConfigurationSource;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;

import com.formacion.juegoscrud.service.impl.UserService;

@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter{

	@Autowired
	private UserService userService;
	
	@Autowired
	private BCryptPasswordEncoder encoder;
	
	@Bean
	public BCryptPasswordEncoder passwordEncoder() {
		BCryptPasswordEncoder  bCryptPasswordEncoder = new BCryptPasswordEncoder();
		return bCryptPasswordEncoder;
	}

	@Override // para guardar la contraseña en la BBDD
	protected void configure(AuthenticationManagerBuilder auth) throws Exception {
			auth.userDetailsService(userService).passwordEncoder(encoder);
	}

	/*@Override
	protected void configure(HttpSecurity http) throws Exception {
		/*http.csrf().disable().httpBasic().and().authorizeRequests().anyRequest().permitAll(); 

		http.authorizeRequests()
			.antMatchers("/cliente").hasAuthority("ADMIN")
			//.antMatchers("/agregarClienteBD").hasAuthority("ADMIN")
			//.antMatchers("/modcliente").hasAuthority("ADMIN")
			//.antMatchers("/bajacliente").hasAuthority("ADMIN")
			.and().csrf().disable().httpBasic();
		
		http.authorizeRequests()
		.antMatchers("/clientes/**").authenticated()
		.and().csrf().disable().httpBasic();
	}*/
	
	//---------------------------------------------
	@Override
	protected void configure(HttpSecurity http) throws Exception {
	    http.authorizeRequests()
		    .antMatchers("/login").permitAll()
		    .antMatchers("/juego/**").permitAll()
            .antMatchers("/company/**").permitAll()
            .antMatchers("/tienda/**").permitAll()
            .antMatchers("/stock/**").permitAll()
            .antMatchers("/roles/**").permitAll()
            .antMatchers("/cliente").fullyAuthenticated()
            .antMatchers("/cliente/**").permitAll()
            .anyRequest().authenticated().and().httpBasic().and().cors()
            .configurationSource(corsConfigurationSource());
	
	    http.csrf().disable();
	    http.headers().frameOptions().disable();
	}
	
	@Bean
	public CorsConfigurationSource corsConfigurationSource() {
	    CorsConfiguration corsConfig = new CorsConfiguration();
	
	    corsConfig.setAllowedOrigins(Arrays.asList("*"));
	    corsConfig.setAllowedMethods(Arrays.asList("POST", "PUT", "GET", "DELETE", "OPTIONS"));
	    corsConfig.setAllowCredentials(true);
	    corsConfig.addAllowedHeader("*");
	
	    UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
	    source.registerCorsConfiguration("/**", corsConfig);
	    return source;
	}
	//---------------------------------------------

	
	/*@Autowired
    public void configureGlobalSecurity(AuthenticationManagerBuilder auth) throws Exception {
        auth
        	.inMemoryAuthentication()
        		.withUser("admin")
        		.password(passwordEncoder().encode("admin"))
        		.roles("USER", "ADMIN");
    }*/
	
   /* @Override
	protected void configure(HttpSecurity http) throws Exception {
		http.authorizeRequests().anyRequest().authenticated().and().csrf().disable().httpBasic();
	}*/

	/**/

    /*@Override
    protected void configure(HttpSecurity http) throws Exception {
      http.csrf().disable().httpBasic().and().authorizeRequests().anyRequest().authenticated();
    }*/
	
}
